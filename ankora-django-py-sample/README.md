# Ankora Django sample API

## Installation

2. `cd` into `sample`
3. Install virtualenv.
4. Install pip3
5. Install Python 3.7
6. Create a new virtualenv `virtualenv venv`.
7. Activate virtualenv: `source venv/bin/activate`.
8. Install requirements from requirements.txt `pip install -r requirements.txt`

### Migrations

1. Run migration `python manager.py migrate`
2. Run server `python manager.py runserver`
