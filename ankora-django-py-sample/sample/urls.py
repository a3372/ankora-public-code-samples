""" URL Configuration """
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^api/', include('sample.apps.authentication.urls', namespace='authentication')),
    url(r'^api/', include('sample.apps.news.urls', namespace='news')),
    url(r'^api/', include('sample.apps.account.urls', namespace='account')),
]
