from django.apps import AppConfig

class AuthAppConfig(AppConfig):
    name = 'sample.apps.authentication'
    label = 'authentication'
    verbose_name = 'Authentication'

# Register App
default_app_config = 'sample.apps.authentication.AuthAppConfig'
