import jwt
from django.conf import settings
from rest_framework import authentication, exceptions
from .models import User

class JWTAuthentication(authentication.BaseAuthentication):
    authentication_header_prefix = 'Token'

    def authenticate(self, request):
        """ The `authenticate` method is called on every request """
        request.user = None

        # Authorization token contains two strings separated with space 'Token <JWT>'
        auth_header = authentication.get_authorization_header(request).split()
        auth_header_prefix = self.authentication_header_prefix.lower()

        # Token invalid or not provided
        if not auth_header or len(auth_header) == 1 or len(auth_header) > 2:
            return None

        # Decode `prefix` Token and `token` <JWT>.
        prefix = auth_header[0].decode('utf-8')
        token = auth_header[1].decode('utf-8')

        # The auth header prefix invalid
        if prefix.lower() != auth_header_prefix:
            return None

        # Return user and token
        return self._validate_token(request, token)

    def _validate_token(self, request, token):
        """ Validate token and return user and token """
        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
        except:
            raise exceptions.AuthenticationFailed('Unauthorized. Token is not valid.')

        try:
            user = User.objects.get(pk=payload['id'])
        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed('Unauthorized. Token is not valid.')

        return (user, token)
