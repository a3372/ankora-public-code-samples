from django.contrib.auth import authenticate
from rest_framework import serializers
from sample.apps.account.serializers import AccountSerializer
from sample.apps.account.models import Account
from .models import User

class RegistrationSerializer(serializers.ModelSerializer):
    """Register new user"""

    name = serializers.CharField(
        max_length=150,
        min_length=5,
        required=True
    )
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )

    token = serializers.CharField(max_length=255, read_only=True)

    class Meta:
        model = User
        # List all of the fields that could possibly be included in a request
        fields = ['email', 'username', 'password', 'name', 'token']

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        if user: 
            user.account = Account.objects.create(user=user)
        return user

class LoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        email = data.get('email', None)
        password = data.get('password', None)

        if email is None or password is None:
            raise serializers.ValidationError(
                'An email and password are required.'
            )

        user = authenticate(username=email, password=password)
        if user is None:
            raise serializers.ValidationError(
                'A user with this email and password was not found.'
            )

        return {
            'email': user.email,
            'token': user.token
        }


class UserSerializer(serializers.ModelSerializer):
    """Handles serialization and deserialization of User objects."""
    
    name = serializers.CharField( min_length=5, max_length=150)
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )
    account = AccountSerializer(write_only=True)
    about = serializers.CharField(source='account.about', read_only=True)

    class Meta:
        model = User
        fields = (
            'email', 'password','username', 'token', 'name', 'account', 'about'
        )
        read_only_fields = ('token',)


    def update(self, instance, validated_data):
        """ Update authenticated user"""

        # Remove fields that should not be updated
        password = validated_data.pop('password', None)
        account_data = validated_data.pop('account', None)

        for (key, value) in account_data.items():
            setattr(instance.account, key, value)
        instance.account.save()

        for (key, value) in validated_data.items():
            setattr(instance, key, value)

        if password is not None:
            instance.set_password(password)

        instance.save()
        

        return instance
