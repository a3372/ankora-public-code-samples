import jwt
from django.db import models
from sample.apps.core.models import TimestampedModel
from datetime import datetime, timedelta
from django.conf import settings
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin
)

class UserManager(BaseUserManager):
    ''' User model '''

    def create_user(self, username, email, name, password=None):
        """Create and return standard `User` with an email, username, name and password."""
        if username is None or email is None or name is None:
            raise TypeError('User must have a username, name and an email address.')

        user = self.model(username=username, email=self.normalize_email(email), name=name)
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, username, email, password):
      """ Create an admin """
      if password is None:
          raise TypeError('Password is required.')

      user = self.create_user(username, email, username, password )
      user.is_staff = True
      user.is_superuser = True
      user.save()
      return user


class User(AbstractBaseUser, PermissionsMixin, TimestampedModel):
    ''' User fields: username, email, name'''
    username = models.CharField(db_index=True, max_length=255, unique=True)
    email = models.EmailField(db_index=True, unique=True)
    name = models.CharField(max_length=150)
    is_staff = models.BooleanField(default=False)

    #  User should login with email, which is specified in USERNAME_FIELD
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']
   
    # Tells Django that the UserManager class defined above should manage objects of this type.
    objects = UserManager()

    def __str__(self):
        """ String representation of User """
        return self.email

    def get_short_name(self):
        return self.username
        
    def get_full_name(self):
      return self.name

    @property
    def token(self):
        """ Fetch users tokens """
        return self._generate_jwt_token()


    def _generate_jwt_token(self):
        """ Generate JWT TOken """
        expiration_time = datetime.now() + timedelta(days=7)
        token = jwt.encode({
            'id': self.pk,
            'exp': int(expiration_time.strftime('%s'))
        }, settings.SECRET_KEY, algorithm='HS256')

        return token.decode('utf-8')
