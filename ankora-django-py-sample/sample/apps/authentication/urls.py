from django.conf.urls import url

from .views import (
    LoginAPIView, RegistrationAPIView, UserAPIView
)

urlpatterns = [
    url(r'^me/?$', UserAPIView.as_view()),
    url(r'^auth/register/?$', RegistrationAPIView.as_view()),
    url(r'^auth/login/?$', LoginAPIView.as_view()),
]
