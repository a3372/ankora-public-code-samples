

from django.apps import AppConfig

class NewsAppConfig(AppConfig):
    name = 'sample.apps.news'
    label = 'news'
    verbose_name = 'News'

    def ready(self):
        from .models import News
        import json
        import requests

        # TODO: Move API key to environment variables
        news_data = requests.get(
            'https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=ece95912ea3746e68826c8eb30e2eb66')
        sample = json.loads(json.dumps(news_data.json()))
        
        for article in sample['articles'][:15]:
            '''
             1. move mapper to helper function
             2. Check if slug already exists
             '''

            try:
                data = {
                    'source': article['source']['name'] or '', 
                    'url': article['url'],
                    'title': article['title'],
                    'description': article['description'],
                    'urlToImage': article['urlToImage'],
                    'body': article['content'],
                    'description': article['description'],
                    'slug': ''.join(c.lower() for c in article['title'] if not c.isspace())
                }
                newNews = News(**data)
                newNews.save()
            except Exception as e: 
                print(e)
            

default_app_config = 'sample.apps.news.NewsAppConfig'
