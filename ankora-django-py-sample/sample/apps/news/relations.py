from rest_framework import serializers
from .models import Topic

class TopicField(serializers.RelatedField):
    def get_queryset(self):
        return Topic.objects.all()

    def to_internal_value(self, data):
        topic, created = Topic.objects.get_or_create(topic=data, slug=data.lower())
        return topic

    def to_representation(self, value):
        return value.name

