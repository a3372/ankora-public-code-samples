from django.db import models

from sample.apps.core.models import TimestampedModel

class News(TimestampedModel):
    slug = models.SlugField(db_index=True, max_length=255, unique=True)
    title = models.CharField(db_index=True, max_length=255)
    description = models.TextField()
    body = models.TextField()
    url = models.CharField(max_length=255)
    urlToImage = models.TextField()
    source = models.CharField(max_length=255)

    topics = models.ManyToManyField(
        'news.Topic', related_name='news'
    )

    def __str__(self):
        return self.title

class Comment(TimestampedModel):
    text = models.TextField()
    author = models.ForeignKey(
        'account.Account', related_name='comments', on_delete=models.CASCADE
    )
    news = models.ForeignKey(
        'news.News', related_name='comments', on_delete=models.CASCADE
    )

class Topic(TimestampedModel):
    name = models.CharField(max_length=100)
    slug = models.SlugField(db_index=True, unique=True)

    def __str__(self):
        return self.name
