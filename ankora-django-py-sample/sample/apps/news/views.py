from django.shortcuts import render
from rest_framework import generics, mixins, status, viewsets
from rest_framework.exceptions import NotFound
from rest_framework.permissions import (
    AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
)
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import News, Topic, Comment
from .serializers import NewsSerializer, TopicSerializer, CommentSerializer
from rest_framework.renderers import JSONRenderer

# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the News index.")

class NewsView(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    lookup_fields = ['slug']
    queryset = News.objects.all()
    permission_classes = (IsAuthenticatedOrReadOnly, )
    renderer_classes = (JSONRenderer, )
    serializer_class = NewsSerializer

    def filter_queryset(self, queryset):
        topic = self.request.query_params.get('topic', None)
        if topic is not None:
            queryset = queryset.filter(topics__name=topic)

        saved_by = self.request.query_params.get('saved', None)
        if saved_by is not None:
            queryset = queryset.filter(
                saved_by__user__username=saved_by
            )

        return queryset

    def get_queryset(self):
        queryset = self.queryset
        queryset = self.filter_queryset(queryset)  # Apply any filter backends
        return queryset

    def create(self, request):
        context = { 'request': request}
        post_data = request.data.get('news', {})
        serializer = self.serializer_class(data=post_data, context=context)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def get_object(self):
        queryset = self.get_queryset()
        filter = {}
        for field in self.multiple_lookup_fields:
            filter[field] = self.kwargs[field]

        obj = get_object_or_404(queryset, **filter)
        return obj
        
    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())
        serializer = self.serializer_class(
            page,
            context=serializer_context,
            many=True
        )

        return self.get_paginated_response(serializer.data)

    def retrieve(self, request, slug):
        serializer_context = {'request': request}

        try:
            serializer_instance = self.queryset.get(slug=slug)
        except News.DoesNotExist:
            raise NotFound('News does not exist')

        serializer = self.serializer_class(
            serializer_instance,
            context=serializer_context
        )

        return Response(serializer.data, status=status.HTTP_200_OK)


    def update(self, request, id):
        serializer_context = {'request': request}

        try:
            serializer_instance = self.queryset.get(id=id)
        except News.DoesNotExist:
            raise NotFound('News does not exist.')
            
        update_data = request.data.get('news', {})

        serializer = self.serializer_class(
            serializer_instance, 
            context=serializer_context,
            data=update_data, 
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


class NewsSaveAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)
    serializer_class = NewsSerializer

    ''' Save news '''
    def post(self, request, news_slug=None):
        account = self.request.user.account
        serializer_context = {'request': request}

        try:
            news = News.objects.get(slug=news_slug)
        except News.DoesNotExist:
            raise NotFound('News not found.')

        account.save_news(news)
        serializer = self.serializer_class(news, context=serializer_context)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    ''' Unsave news '''
    def delete(self, request, news_slug=None):
        account = self.request.user.account
        serializer_context = {'request': request}

        try:
            news = News.objects.get(slug=news_slug)
        except News.DoesNotExist:
            raise NotFound('News not found.')

        account.unsave(news)
        serializer = self.serializer_class(news, context=serializer_context)

        return Response(serializer.data, status=status.HTTP_200_OK)


class NewsSavedAPIViews(generics.ListAPIView):
    ''' Fetch all topics that news is related to '''
    permission_classes = (IsAuthenticated,)
    queryset = News.objects.all()
    pagination_class = None
    serializer_class = NewsSerializer
    renderer_classes = (JSONRenderer, )

    def list(self, request):
        serializer_data = self.get_queryset()
        serializer = self.serializer_class(serializer_data, many=True)
        return Response({
            'news': serializer.data
        }, status=status.HTTP_200_OK)


class CommentCreateAPIView(generics.ListCreateAPIView):
    lookup_field = 'news__slug'
    lookup_url_kwarg = 'news_slug'
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = Comment.objects.select_related(
        'news', 'author', 'author__user'
    )
    renderer_classes = (JSONRenderer,)
    serializer_class = CommentSerializer

    def filter_queryset(self, queryset):
        filters = {self.lookup_field: self.kwargs[self.lookup_url_kwarg]}
        return queryset.filter(**filters)

    def create(self, request, news_slug=None):
        data = request.data.get('comment', {})
        context = {'author': request.user.account}

        try:
            context['news'] = News.objects.get(slug=news_slug)
        except News.DoesNotExist:
            raise NotFound('News not found.')

        serializer = self.serializer_class(data=data, context=context)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class TopicAPIView(generics.ListAPIView):
    ''' Fetch all topics that news is related to '''
    queryset = Topic.objects.all()
    pagination_class = None
    permission_classes = (AllowAny,)
    serializer_class = TopicSerializer

    def list(self, request):
        serializer_data = self.get_queryset()
        serializer = self.serializer_class(serializer_data, many=True)
        return Response({
            'topics': serializer.data
        }, status=status.HTTP_200_OK)
