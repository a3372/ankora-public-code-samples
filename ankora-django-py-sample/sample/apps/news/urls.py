from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from .views import NewsView, NewsSaveAPIView, TopicAPIView, CommentCreateAPIView

router = DefaultRouter(trailing_slash=False)
router.register(r'news', NewsView)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^news/save/(?P<news_slug>[-\w]+)/?$', NewsSaveAPIView.as_view()),
    url(r'^news/comments/(?P<news_slug>[-\w]+)?$', CommentCreateAPIView.as_view()),
    url(r'^topics/?$', TopicAPIView.as_view()),
]

