from rest_framework import serializers
from sample.apps.account.serializers import AccountSerializer
from .relations import TopicField

from .models import News, Topic, Comment

class CommentSerializer(serializers.ModelSerializer):
    author = AccountSerializer(required=False)
    createdAt = serializers.SerializerMethodField(method_name='get_time_created')
    updatedAt = serializers.SerializerMethodField(method_name='get_time_update')

    class Meta:
        model = Comment
        fields = (
            'id',
            'author',
            'text',
            'createdAt',
            'updatedAt',
        )

    def create(self, validated_data):
        news = self.context['news']
        author = self.context['author']

        return Comment.objects.create(
            author=author, news=news, **validated_data
        )

    def get_time_created(self, instance):
        return instance.created_at.isoformat()

    def get_time_update(self, instance):
        return instance.updated_at.isoformat()


class NewsSerializer(serializers.ModelSerializer):
    description = serializers.CharField(required=False)
    slug = serializers.SlugField(required=False)
    title = serializers.CharField(required=False)
    url = serializers.CharField(required=True)
    urlToImage = serializers.CharField(required=False)
    source = serializers.CharField(required=False)
    body = serializers.CharField(required=True)
    createdAt = serializers.SerializerMethodField(method_name='get_created_at')
    updatedAt = serializers.SerializerMethodField(method_name='get_updated_at')
    saved = serializers.SerializerMethodField()
    savedCount = serializers.SerializerMethodField(
        method_name='get_saved_count'
    )

    allComments = CommentSerializer(many=True, required=False, source='comments')
    all_topics = TopicField(many=True, required=False, source='topics')

    class Meta:
        model = News
        fields = (
            'body',
            'createdAt',
            'description',
            'saved',
            'savedCount',
            'slug',
            'title',
            'updatedAt',
            'url',
            'source',
            'urlToImage',
            'all_topics',
            'allComments',
        )

    def create(self, validated_data):
        topics = validated_data.pop('topics', [])
        news = News.objects.create(**validated_data)

        for topics in topics:
            news.topics.add(tag)

        return news

    def get_created_at(self, instance):
        return instance.created_at.isoformat()

    def get_saved(self, instance):
        request = self.context.get('request', None)

        if request is None:
            return False

        if not request.user.is_authenticated():
            return False

        return request.user.account.has_saved(instance)

    def get_saved_count(self, instance):
        return instance.saved_by.count()

    def get_updated_at(self, instance):
        return instance.updated_at.isoformat()


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = ('topic',)

    def to_representation(self, obj):
        return obj.name
