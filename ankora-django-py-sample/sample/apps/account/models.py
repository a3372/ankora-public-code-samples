from django.db import models
from sample.apps.core.models import TimestampedModel

class Account(TimestampedModel):
    ''' Account is connected with User table with One on One relationshop '''
    user = models.OneToOneField(
        'authentication.User', on_delete=models.CASCADE
    )

    about = models.TextField(blank=True)
    address = models.TextField(blank=True)
    image = models.URLField(blank=True)

    # User should be able to save news 
    saved = models.ManyToManyField('news.News', related_name='saved_by')

    def __str__(self):
        ''' User / account string representation because username is'''
        return self.user.username

    def save_news(self, news):
        """ Save news news """
        self.saved.add(news)

    def unsave(self, news):
        """ Unsave news news """
        self.saved.remove(news)

    def has_saved(self, news):
        """ Returns True if we have saved `news`; else False. """
        return self.saved.filter(pk=news.pk).exists()
