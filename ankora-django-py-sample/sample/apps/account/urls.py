from django.conf.urls import url
from .views import AccountAPIView

urlpatterns = [
    url(r'^account/(?P<username>\w+)/?$', AccountAPIView.as_view()),
]
