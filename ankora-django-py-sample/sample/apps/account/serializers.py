from rest_framework import serializers
from .models import Account

class AccountSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')
    about = serializers.CharField(allow_blank=True, required=False)
    image = serializers.SerializerMethodField()
    address = serializers.CharField(allow_blank=True, required=False)

    class Meta:
        model = Account
        fields = ('username', 'about', 'address', 'image')
        read_only_fields = ('username',)

    def get_image(self, obj):
        if obj.image:
            return obj.image
        return 'https://w7.pngwing.com/pngs/858/581/png-transparent-profile-icon-user-computer-icons-system-chinese-wind-title-column-miscellaneous-service-logo.png'

