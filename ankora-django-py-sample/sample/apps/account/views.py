from rest_framework import serializers, status
from rest_framework.exceptions import NotFound
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from .models import Account
from .serializers import AccountSerializer

class AccountAPIView(RetrieveAPIView):
    permission_classes = (AllowAny,)
    queryset = Account.objects.select_related('user')
    renderer_classes = (JSONRenderer,)
    serializer_class = AccountSerializer

    # GET Account
    def retrieve(self, request, username, *args, **kwargs):
        try:
            account = self.queryset.get(user__username=username)
        except Account.DoesNotExist:
            raise NotFound('Username does not exist')

        serializer = self.serializer_class(account, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)
