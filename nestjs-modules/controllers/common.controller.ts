import {Param, Body, Controller, Get, Post, UseGuards} from '@nestjs/common';
import {CommonService} from './common.service';
import {TestLoggerResponseDto} from './testLogger.dto';
import {SharedErrors} from '../../core/error/shared.error';
import {ApiTags} from '@nestjs/swagger';
import {AuthGuard} from '../auth/auth.guard';

@ApiTags('Dev Support')
@Controller()
export class CommonController {
  constructor(private readonly commonService: CommonService) {}

  @UseGuards(AuthGuard)
  @Get('test-logger')
  public async testLogger(): Promise<TestLoggerResponseDto> {
    return this.commonService.testLogger();
  }

  @Get('test-dummy-get')
  public async testDummyGet(@Param('dummyId') dummyId?: string): Promise<{id: string; name: string}> {
    return this.commonService.testDummyGet(dummyId);
  }

  @Post('test-dummy-post')
  public async testDummyPost(@Body('throwError') throwError: boolean): Promise<{id: string; name: string}> {
    if (throwError) {
      throw new SharedErrors.BadRequestError();
    }
    return this.commonService.testDummyPost();
  }
}
