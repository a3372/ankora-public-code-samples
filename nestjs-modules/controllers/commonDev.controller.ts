import {ClassSerializerInterceptor, Controller, Get, UseGuards, UseInterceptors} from '@nestjs/common';
import {ApiBearerAuth, ApiTags} from '@nestjs/swagger';

import {DEVSUPPORT_ENDPOINT} from '../../app.constants';
import {AdvisoryLock} from '../../core/advisoryLock/advisoryLock';
import {DevOnlyGuard} from '../../core/guards/devOnly.guard';
import {ReleaseToggleHelper} from '../../helpers/releaseToggle.helper';

@UseGuards(DevOnlyGuard)
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
@Controller(`${DEVSUPPORT_ENDPOINT}/common`)
@ApiTags('Dev Support')
export class CommonDevsupportController {
  constructor(private readonly advisoryLock: AdvisoryLock, private readonly releaseToggle: ReleaseToggleHelper) {}
  @Get('advisory-lock')
  public async DEV_getMany() {
    const gotLock = await this.advisoryLock.tryDoWithLock('DEV_LOCK_ID', async () => {
      return new Promise((resolve) => setTimeout(resolve, 3000));
    });

    return {gotLock};
  }

  @Get('release-toggles-example')
  public DEV_testReleaseToggles() {
    if (!this.releaseToggle.EXAMPLE_featureEnabledByEnv()) {
      return {};
    }

    return {
      status: 'Works only on nonprod envs',
    };
  }
}
