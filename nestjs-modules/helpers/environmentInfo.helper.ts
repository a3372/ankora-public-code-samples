import {Injectable} from '@nestjs/common';
import {AppConfigService} from '../core/config/appConfig.service';

@Injectable()
export class EnvironmentInfoHelper {
  constructor(private readonly appConfig: AppConfigService) {}

  public get isProductionZone(): boolean {
    return ['prod', 'uat'].includes(this.appConfig.env);
  }

  public get isNonProductionZone(): boolean {
    return !this.isProductionZone;
  }

  public get isLocalHost(): boolean {
    return this.appConfig.env === 'local';
  }
}
