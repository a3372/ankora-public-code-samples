import {Injectable} from '@nestjs/common';
import {EnvironmentInfoHelper} from './environmentInfo.helper';

@Injectable()
export class ReleaseToggleHelper {
  constructor(private readonly envHelper: EnvironmentInfoHelper) {}

  public EXAMPLE_featureEnabledByEmail = (email: string) => {
    const ENABLED_EMAILS = ['nedim@ankorainc.com'];

    return ENABLED_EMAILS.indexOf(email) >= 0;
  };

  public EXAMPLE_featureEnabledByEnv = () => {
    return this.envHelper.isNonProductionZone;
  };
}
