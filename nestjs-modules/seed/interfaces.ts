import {Feature} from '../src/modules/featureConfig/featureConfig.constants';


export interface SeedSpecialization {
  id: string;
  name: string;
}

export interface SeedFeatureConfig {
  feature: Feature;
  config: Record<string, unknown>;
}
