export const isProd = () => ['production', 'prod', 'prd', 'uat'].includes(process.env.NODE_ENV);
