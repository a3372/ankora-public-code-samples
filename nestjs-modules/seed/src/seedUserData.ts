import {User} from '../../src/modules/user/user.entity';
import {UserRepository} from '../../src/modules/user/user.repository';
import {getSeedData} from './getSeedData';

export const seedUser = async (db) => {
  const seedData = getSeedData('User') as Partial<User>[];
  const count = seedData.length;
  if (!count) {
    return;
  }

  console.log('\x1b[33m%s\x1b[0m', `Seeding Users (${count} items)`);
  const repository: UserRepository = db.getCustomRepository(UserRepository);
  await Promise.all(
    seedData.map(async (data) => {
      let user = await repository.findOneByPrimaryKey(data.id);
      if (!user) {
        user = new User();
      }
      Object.assign(user, data);
      return repository.save(user);
    }),
  );
};
