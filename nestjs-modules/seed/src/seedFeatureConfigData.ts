import {Connection} from 'typeorm';
import {FeatureConfig} from '../../src/modules/featureConfig/featureConfig.entity';
import {FeatureConfigRepository} from '../../src/modules/featureConfig/featureConfig.repository';
import {SeedFeatureConfig} from '../interfaces';
import {getSeedData} from './getSeedData';

export const seedFeatureConfigData = async (db: Connection) => {
  const seedData = getSeedData('FeatureConfig') as SeedFeatureConfig[];
  const count = seedData.length;
  if (!count) {
    return;
  }

  console.log('\x1b[33m%s\x1b[0m', `Seeding Feature configuration (${count} items)`);
  const repository: FeatureConfigRepository = db.getCustomRepository(FeatureConfigRepository);
  await Promise.all(
    seedData.map(async (data) => {
      let featureConfigEntry = await repository.getOneByFeatureName(data.feature);
      if (!featureConfigEntry) {
        featureConfigEntry = new FeatureConfig();
      }
      Object.assign(featureConfigEntry, data);
      return repository.save(featureConfigEntry);
    }),
  );
};
