import {Connection} from 'typeorm';
import {Company} from '../../src/modules/company/company.entity';
import {CompanyRepository} from '../../src/modules/company/company.repository';
import {CompanyDto} from '../../src/modules/company/dto/company.dto';
import {getSeedData} from './getSeedData';

export const seedCompany = async (db: Connection) => {
  const seedData = getSeedData('Company') as CompanyDto[];
  const count = seedData.length;
  if (!count) {
    return;
  }

  console.log('\x1b[33m%s\x1b[0m', `Seeding Company (${count} items)`);
  const repository: CompanyRepository = db.getCustomRepository(CompanyRepository);
  await Promise.all(
    seedData.map(async (data) => {
      let company = await repository.findOneByPrimaryKey(data.id);
      if (!company) {
        company = new Company();
      }
      Object.assign(company, data);
      return repository.save(company);
    }),
  );
};
