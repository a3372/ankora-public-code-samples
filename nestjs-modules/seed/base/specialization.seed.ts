/* ID naming convention
00000000-0000-4000-a000-00000000000* => userId
000000000000000000000000000* => firebaseId
00000000-0000-4000-a000-0000000000** => companyId
00000000-0000-4000-a000-000000000*** => specializationId
*/

import {SeedSpecialization} from 'seed/interfaces';

export const specializationSeed: SeedSpecialization[] = [
  {
    id: `00000000-0000-4000-a000-000000000100`,
    name: 'Engineering',
  },
  {
    id: `00000000-0000-4000-a000-000000000101`,
    name: 'Design',
  },
  {
    id: `00000000-0000-4000-a000-000000000102`,
    name: 'Product',
  },
  {
    id: `00000000-0000-4000-a000-000000000103`,
    name: 'Marketing & sales',
  },
  {
    id: `00000000-0000-4000-a000-000000000104`,
    name: 'Data Science',
  },
  {
    id: `00000000-0000-4000-a000-000000000105`,
    name: 'HR, TA, recruiting',
  },
  {
    id: `00000000-0000-4000-a000-000000000106`,
    name: 'Customer support',
  },
  {
    id: `00000000-0000-4000-a000-000000000107`,
    name: 'Operations',
  },
  {
    id: `00000000-0000-4000-a000-000000000108`,
    name: 'Finance & Accounting',
  },
  {
    id: `00000000-0000-4000-a000-000000000109`,
    name: 'Legal',
  },
];
