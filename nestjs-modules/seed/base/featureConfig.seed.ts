import {Feature} from '../../src/modules/featureConfig/featureConfig.constants';
import {SeedFeatureConfig} from '../interfaces';

export const featureConfigSeed: SeedFeatureConfig[] = [
  {
    feature: Feature.DEMO_FEATURE,
    config: {
      stringKey: 'foo',
      booleanKey: true,
      complexObject: {
        foo: 'bar',
      },
    },
  },
];
