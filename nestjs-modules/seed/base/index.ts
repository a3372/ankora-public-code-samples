import {featureConfigSeed} from './featureConfig.seed';
import {specializationSeed} from './specialization.seed';

export default {
  Specialization: {
    data: specializationSeed,
  },
  FeatureConfig: {
    data: featureConfigSeed,
  },
};
