/* ID naming convention
00000000-0000-4000-a000-00000000000* => userId
000000000000000000000000000* => firebaseId
00000000-0000-4000-a000-0000000000** => companyId
*/

import {ROLE} from '../../src/constants/auth.enum';
import {User} from '../../src/modules/user/user.entity';

export const userSeed: Partial<User>[] = [
  {
    id: '00000000-0000-4000-a000-000000000000',
    email: 'admin@ankorainc.com',
    firstName: 'Anna',
    lastName: 'Melano',
    firebaseId: '0000000000000000000000000000',
    customerId: null,
    role: ROLE.ADMIN,
  },
  {
    id: '00000000-0000-4000-a000-000000000001',
    email: 'dev@ankorainc.com',
    firstName: 'Nermin',
    lastName: 'Hadzic',
    firebaseId: '0000000000000000000000000001',
    customerId: 'cus_JwT4sFm9T48VlK',
    role: ROLE.COMPANY_ADMIN,
    onboardingStep: 3,
  },
  {
    id: '00000000-0000-4000-a000-000000000002',
    email: 'recruiter@ankorainc.com',
    firstName: 'Tony',
    lastName: 'AI',
    firebaseId: '0000000000000000000000000001',
    customerId: null,
    role: ROLE.RECRUITER,
    onboardingStep: 2,
  },
  {
    id: '00000000-0000-4000-a000-000000000003',
    email: 'eng@ankorainc.com',
    firstName: 'Khaled',
    lastName: 'Hussein',
    firebaseId: '0000000000000000000000000001',
    customerId: null,
    role: ROLE.COMPANY_ADMIN,
    onboardingStep: 3,
  },
  {
    id: '00000000-0000-4000-a000-000000000004',
    email: 'recruiter1@ankorainc.com',
    firstName: 'TonyTony',
    lastName: 'Tan',
    firebaseId: '0000000000000000000000000001',
    customerId: null,
    role: ROLE.RECRUITER,
  },
  {
    id: '00000000-0000-4000-a000-000000000005',
    email: 'dev2@ankorainc.com',
    firstName: 'Faruk',
    lastName: 'Smajlovic',
    firebaseId: '0000000000000000000000000001',
    customerId: 'cus_JwT4sFm9T48VlK',
    role: ROLE.COMPANY_ADMIN,
    onboardingStep: 3,
  },
];
