/* ID naming convention
00000000-0000-4000-a000-00000000000* => userId
000000000000000000000000000* => firebaseId
00000000-0000-4000-a000-0000000000** => companyId
*/

import {LAST_ROUND_RAISED, POST_COVID, RAISED_AMOUNT, TEAM_SIZE} from '../../src/constants/company.enum';
import {CompanyDto} from '../../src/modules/company/dto/company.dto';

export const companySeed: CompanyDto[] = [
  {
    id: `00000000-0000-4000-a000-000000000011`,
    companyName: 'ankora. Software Development',
    companyUrl: 'https://www.ankorainc.com',
    teamSize: TEAM_SIZE.M,
    lastRoundRaised: LAST_ROUND_RAISED.BOOTSTRAPPED,
    amountMoneyRaisedToDate: RAISED_AMOUNT.S,
    notableInvestors: ['PartnerHub', 'AnkoraINC'],
    locationHQ: 'Alojza Benca 4, 71000 Sarajevo',
    postCovidPlan: POST_COVID.HYBRID,
    perks: {},
  },
  {
    id: `00000000-0000-4000-a000-000000000012`,
    companyName: 'AnkoraINC',
    companyUrl: 'https://www.ankorainc.com',
    teamSize: TEAM_SIZE.M,
    lastRoundRaised: LAST_ROUND_RAISED.BOOTSTRAPPED,
    amountMoneyRaisedToDate: RAISED_AMOUNT.S,
    notableInvestors: ['Airbnb'],
    locationHQ: 'San Francisco',
    postCovidPlan: POST_COVID.HYBRID,
    perks: {},
  },
  {
    id: `00000000-0000-4000-a000-000000000013`,
    companyName: 'Inactive company',
    companyUrl: 'https://www.inactive.com',
    teamSize: TEAM_SIZE.M,
    lastRoundRaised: LAST_ROUND_RAISED.BOOTSTRAPPED,
    amountMoneyRaisedToDate: RAISED_AMOUNT.S,
    notableInvestors: ['Airbnb'],
    locationHQ: 'San Francisco',
    postCovidPlan: POST_COVID.HYBRID,
    perks: {},
  },
];
