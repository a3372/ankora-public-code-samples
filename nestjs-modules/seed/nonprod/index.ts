import {companySeed} from './company.seed';
import {companyAdminSeed} from './companyAdmin.seed';
import { companyBillingInfo } from './companyBillingInfo.seed';
import {recruiterSeed} from './recruiter.seed';
import { roleSeed } from './role.seed';
import {userSeed} from './user.seed';

export default {
  Company: {
    data: companySeed,
    overwrite: true,
  },
  User: {
    data: userSeed,
    overwrite: true,
  },
};
