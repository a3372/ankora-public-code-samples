import { NestFactory } from '@nestjs/core';
import { Connection } from 'typeorm';
import { AppModule } from '../src/app.module';
import { seedCompany } from './src/seedCompanyData';
import { seedSpecialization } from './src/seedSpecializationData';
import { seedUser } from './src/seedUserData';

const seed = async () => {
  console.log(`Seed Starting #${process.pid}`);
  const app = await NestFactory.create(AppModule, {
    logger: ['error', 'warn'],
  });

  app.useLogger(null);
  const db = app.get<Connection>(Connection);

  await seedCompany(db);
  await seedUser(db);
  await seedSpecialization(db);
};

seed().then(
  () => process.exit(0),
  (err) => {
    console.error(err.stack);
    process.exit(1);
  },
);
