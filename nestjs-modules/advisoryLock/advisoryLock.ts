import {Injectable} from '@nestjs/common';
import {createHash} from 'crypto';
import qs from 'querystring';
import {Client, QueryResult} from 'pg';
import {AppConfigService} from '../config/appConfig.service';
import {Logger} from '../logger';

@Injectable()
export class AdvisoryLock {
  constructor(private readonly logger: Logger, private readonly appConfig: AppConfigService) {}

  private async mutex(lockId: string) {
    const {host, port, username, password, database, ssl} = this.appConfig.dbConfig;

    const queryObj: Record<string, string> = {};
    if (ssl) {
      queryObj.ssl = ssl;
    }
    const q = qs.stringify(queryObj);

    const connectionString = `postgres://${username}:${password}@${host}:${port}/${database}?${q}`;
    const client = new Client({connectionString});
    await client.connect();

    const query = (lockFn: string): Promise<boolean> =>
      new Promise(function (resolve, reject) {
        const hash = createHash('sha256').update(lockId).digest();
        const values = [hash.readInt32LE(0), hash.readInt32LE(4)];
        const sql = `SELECT ${lockFn} ($1, $2)`;
        const callback = (err: Error, result: QueryResult) => (err ? reject(err) : resolve(result.rows[0][lockFn]));
        client.query(sql, values, callback);
      });

    const tryLock = (): Promise<boolean> => query('pg_try_advisory_lock');
    const unlock = (): Promise<boolean> => query('pg_advisory_unlock');
    const dispose = async (): Promise<void> => {
      await unlock().catch((e) => {
        this.logger.error(__filename, `error ${e.stack || e} unlocking lockId: ${lockId}`);
      });
      await client.end();
    };

    return {
      tryLock,
      unlock,
      dispose,
    };
  }

  public async tryDoWithLock(lockId: string, doWithLock: () => Promise<void>): Promise<boolean> {
    this.logger.info(__filename, 'AdvisoryLock.tryDoWithLock: lockId: ' + lockId);
    const {tryLock, dispose} = await this.mutex(lockId);

    const gotLock = Boolean(await tryLock().catch(dispose));
    try {
      if (gotLock) {
        await doWithLock();
      }
    } finally {
      await dispose();
    }

    return gotLock;
  }
}
