import {Injectable} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';
import {IApiDocumentation} from './interfaces/apiDocumentation.interface';

@Injectable()
export class AppConfigService {
  constructor(private configService: ConfigService) {}

  public get env(): string {
    return this.configService.get('NODE_ENV');
  }

  public get appUrl(): string {
    return this.configService.get('appUrl');
  }

  public get serverPort(): number {
    return (
      Number(process.env.PORT) ||
      Number(process.env.APP_PORT) ||
      parseInt(this.configService.get('serverPort'), 10) ||
      4000
    );
  }

  public get dbConfig() {
    return {
      host: this.configService.get('DATABASE_HOST') || this.configService.get('dbConfig.host'),
      port: this.configService.get('DATABASE_PORT') || Number(this.configService.get('dbConfig.port')),
      username: this.configService.get('DATABASE_USER') || this.configService.get('dbConfig.username'),
      password: this.configService.get('DATABASE_PASSWORD') || this.configService.get('dbConfig.password'),
      database: this.configService.get('DATABASE_DB') || this.configService.get('dbConfig.database'),
      ssl: this.configService.get('dbConfig.ssl'),
      cache: this.configService.get('dbConfig.cache'),
      logging: false,
    };
  }

  public get isProductionZone(): boolean {
    return ['p', 'prd', 'production', 'u', 'uat'].includes(this.env);
  }

  public get apiDocumentation(): IApiDocumentation {
    return this.configService.get('apiDocumentation');
  }

  public get firebaseConfig(): Record<string, string> {
    return {
      type: this.configService.get('FIREBASE_TYPE'),
      projectId: this.configService.get('FIREBASE_PROJECT_ID'),
      privateKeyId: this.configService.get('FIREBASE_PRIVATE_KEY_ID'),
      privateKey: this.configService.get('FIREBASE_PRIVATE_KEY'),
      client_email: this.configService.get('FIREBASE_CLIENT_EMAIL'),
      clientId: this.configService.get('FIREBASE_CLIENT_ID'),
      authUri: this.configService.get('FIREBASE_AUTH_URI'),
      tokenUri: this.configService.get('FIREBASE_TOKEN_URI'),
      authProviderX509XertUrl: this.configService.get('FIREBASE_AUTH_PROVIDER'),
      clientX509XertUrl: this.configService.get('FIREBASE_CLIENT_URL'),
    };
  }

  public get adminAppConfig(): {url: string} {
    return {
      url: this.configService.get('appConfig.adminAppUrl'),
    };
  }
  public get frontend() {
    return {
      url: this.configService.get('appConfig.frontendUrl'),
    };
  }
  public get slackConfig(): {url: string} {
    return {
      url: this.configService.get('slackHookUrl')
    };
  }

  public get mailerConfig() {
    return {transport: this.configService.get('MAILER_TRANSPORT') || this.configService.get('mailer')};
  }

  public get awsS3Config() {
    return {
      accessKeyId: this.configService.get('AWS_ACCESS_KEY_ID') || this.configService.get('aws.accessKey'),
      secretAccessKey: this.configService.get('AWS_SECRET_ACCESS_KEY') || this.configService.get('aws.secretAccessKey'),
      bucket: this.configService.get('AWS_S3_BUCKET') || this.configService.get('aws.bucket'),
    };
  }
}
