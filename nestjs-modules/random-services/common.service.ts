import {Injectable} from '@nestjs/common';
import {ContextService} from '../../core/context/context.service';
import {Logger} from '../../core/logger';
import {CommonError} from './common.error';
import {TestLoggerResponseDto} from './testLogger.dto';

@Injectable()
export class CommonService {
  constructor(private readonly logger: Logger, private readonly contextService: ContextService) {}

  public async testLogger(): Promise<TestLoggerResponseDto> {
    let error;
    const data = {
      a: 1,
      b: true,
      c: 'test',
    };

    this.logger.trace(__filename, 'trace message without data');
    this.logger.trace(__filename, 'trace message with data', data);
    this.logger.debug(__filename, 'debug message without data');
    this.logger.debug(__filename, 'debug message with data', data);
    this.logger.info(__filename, 'info message without data');
    this.logger.info(__filename, 'info message with data', data);

    error = new CommonError.CommonServiceIntentionalError({
      message: 'error message without data',
    });
    this.logger.error(error, __filename);

    error = new CommonError.CommonServiceIntentionalError({
      message: 'error message with data',
    });
    error.setData(data);
    this.logger.error(error, __filename);

    error = new CommonError.CommonServiceIntentionalError();
    this.logger.error(error, __filename);

    this.contextService.metaContext = {foo: 'bar'};
    this.contextService.addMeta('who', 'AnkoraINC');

    return {status: 'ok'};
  }

  public async testDummyGet(dummyId?: string): Promise<{id: string; name: string}> {
    return {id: dummyId, name: 'dummyName'};
  }

  public async testDummyPost(): Promise<{id: string; name: string}> {
    return {id: 'idValue', name: 'nameValue'};
  }
}
