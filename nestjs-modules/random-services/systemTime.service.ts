import {Injectable} from '@nestjs/common';
import moment from 'moment';

@Injectable()
export class SystemTimeService {
  private fixedTime: number;

  private simulatedTimeStartPoint: number = 0;

  private commandChangedAt: number = 0;

  public getNow(): moment.Moment {
    if (this.simulatedTimeStartPoint) {
      return this.getSimulationMoment();
    }
    if (this.fixedTime) {
      return this.getFixedMoment();
    }
    return moment();
  }

  public simulateTime(time: Date | moment.Moment | string | number): this {
    this.reset();
    this.simulatedTimeStartPoint = moment(time).valueOf();
    this.commandChangedAt = moment().valueOf();
    return this;
  }

  public fixTime(time: Date | moment.Moment | string | number): this {
    this.reset();
    this.fixedTime = moment(time).valueOf();
    this.commandChangedAt = moment().valueOf();
    return this;
  }

  public reset() {
    this.simulatedTimeStartPoint = 0;
    this.fixedTime = 0;
    this.commandChangedAt = moment().valueOf();
  }

  private getSimulationMoment() {
    const diff = moment().valueOf() - this.commandChangedAt;
    return moment(this.simulatedTimeStartPoint).add(diff, 'ms');
  }

  private getFixedMoment() {
    return moment(this.fixedTime);
  }
}
