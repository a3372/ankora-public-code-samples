import classNames from 'classnames'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import React from 'react'
import scrollDesktop from '../../../assets/svg/scroll-down-desktop.svg'
import scrollMobile from '../../../assets/svg/scroll-down.svg'
import { l } from '../../../public/locales/translation'
import { ROUTES } from '../../../styles/config/route-names'
import AnimatedHeading from '../../animated/heading/AnimatedHeading'
import { rtl } from '../../global/RTLFlag'
import { scrollToId } from '../../helpers/ScrollHelper'
import styles from './Heading.module.scss'

const Button = dynamic(() => import('../../global/button/Button'))

const cn = classNames.bind(styles)

const Heading = (): JSX.Element => {
  const router = useRouter()

  return (
    <div className={styles.heading_container}>
      <div className={cn(styles.heading_wrapper, { [styles.heading_wrapper_rtl]: rtl })}>
        <div className={styles.heading_content}>
          <h1>
            {l.product} <br /> {l.development} & <br />
            {l.designCompany} <br /> {l.workingWith} <br /> {l.startups} & <br /> {l.enterprises}
          </h1>
          <p>{l.madeTheChoice}</p>
          <Button onClick={() => router.push(ROUTES.CONTACT)}>{l.contactUS}</Button>
          <div
            className={cn(styles.heading_scroll_down, { [styles.heading_scroll_down_rtl]: rtl })}
            onClick={() => scrollToId('#services')}
          >
            <img src={scrollDesktop} alt="Scroll down icon for desktop" />
            <p>{l.scrollDown}</p>
          </div>
        </div>
        <div className={styles.heading_image}>
          <AnimatedHeading />
        </div>
        <div className={styles.heading_scroll} onClick={() => scrollToId('#services')}>
          <img src={scrollMobile} alt="Scroll down icon" />
        </div>
      </div>
    </div>
  )
}

export default Heading
