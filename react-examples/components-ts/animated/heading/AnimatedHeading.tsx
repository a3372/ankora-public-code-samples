import classNames from 'classnames'
import { motion, useAnimation, useTransform, useViewportScroll } from 'framer-motion'
import React, { useEffect } from 'react'
import s from './AnimatedHeading.module.scss'
import Cloud from './assets/cloud.png'
import Coffee from './assets/coffee.png'
import Desktop from './assets/desktop.png'
import Phone from './assets/phone.png'

const cn = classNames.bind(s)

const delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms))
const AnimatedHeading = () => {
  const base = useAnimation()
  const coffee = useAnimation()
  const phone = useAnimation()
  const cloud_1 = useAnimation()
  const cloud_2 = useAnimation()
  const { scrollY } = useViewportScroll()
  const cloud1Y = useTransform(scrollY, [0, 500], [0, -50])
  const desktopY = useTransform(scrollY, [0, 500], [0, -100])
  const cloud2Y = useTransform(scrollY, [0, 500], [0, -150])
  const phoneY = useTransform(scrollY, [0, 500], [0, -200])
  const coffeeY = useTransform(scrollY, [0, 500], [0, -300])

  const triggerAnimations = async () => {
    await delay(500)
    base.start({
      opacity: 1,
      transform: `scale(1.2)`,
      transition: {
        duration: 0.3,
      },
    })
    await delay(150)

    coffee
      .start({
        opacity: 1,
        transform: `scale(0.9) translate(-20%, 8%)`,
        left: '-2%',
        top: '23.5%',
        transition: {
          duration: 0.2,
          type: 'linear',
        },
      })
      .then(() => {
        coffee.start({
          transform: `scale(1) translate(-5%, 8%)`,
          transition: {
            duration: 0.5,
            type: 'easeInOut',
            stiffness: 300,
          },
        })
      })

    phone.start({
      opacity: 1,
      transform: `scale(1)`,
    })

    await delay(200)
    cloud_1.start({
      opacity: 1,
      transform: `scale(1)`,
      transition: {
        type: 'spring',
      },
    })
    cloud_2.start({
      opacity: 1,
      transform: `scale(0.8)`,
      transition: {
        type: 'spring',
      },
    })
  }

  useEffect(() => {
    triggerAnimations()
  }, [])
  return (
    <div className={s.wrapper}>
      <motion.div animate={base} style={{ y: desktopY }} className={cn(s.base)}>
        <img src={Desktop} alt="responsive" />
      </motion.div>
      <motion.div animate={cloud_1} style={{ y: cloud1Y }} className={cn(s.cloud_1)}>
        <img src={Cloud} alt="cloud_one" />
      </motion.div>
      <motion.div animate={cloud_2} style={{ y: cloud2Y }} className={cn(s.cloud_2)}>
        <img src={Cloud} alt="cloud_two" />
      </motion.div>
      <motion.div animate={phone} style={{ y: phoneY }} className={cn(s.phone)}>
        <img src={Phone} alt="phone" />
      </motion.div>
      <motion.div animate={coffee} style={{ y: coffeeY }} className={cn(s.coffee)}>
        <img src={Coffee} alt="coffee" />
      </motion.div>
    </div>
  )
}

export default AnimatedHeading
