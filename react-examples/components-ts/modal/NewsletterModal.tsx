import classNames from 'classnames'
import React from 'react'
import NewsletterBackground from '../../assets/png/subscribe-modal-background.png'
import CircleLeft from '../../assets/svg/circle-left.svg'
import { useModal } from '../../hooks/modal'
import Subscribe from '../global/subscribe/Subscribe'
import styles from './Modal.module.scss'

const cn = classNames.bind(styles)

const NewsletterModal = (): JSX.Element => {
  const { hideModal } = useModal()

  return (
    <div className={styles.newsletter_wrapper}>
      <div className={cn(styles.modal_container_back, styles.modal_newsletter_back)} onClick={() => hideModal()}>
        <img src={CircleLeft} alt="button for go back" />
        <h5>BACK TO HOME </h5>
      </div>
      <div className={cn(styles.modal_container, styles.modal_newsletter_subscribe, styles.newsletter_content_spacing)}>
        <Subscribe hideHeader white />
      </div>
      <img className={styles.newsletter_image} src={NewsletterBackground} alt="Newsletter background image" />
    </div>
  )
}

export default NewsletterModal
