import React, { createContext, useState } from 'react'

import { MODALS_CONTENT } from './modals'
import ModalWrapper from './ModalWrapper'

interface IModal {
  key: string | null
  props?: unknown
}
export const ModalContext = createContext<{
  showModal: (key: string, props: unknown) => void
  hideModal: () => void
}>({
  showModal: () => null,
  hideModal: () => null,
})

const ModalProvider = ({ children }: { children: JSX.Element }): JSX.Element => {
  const [modalData, setModalData] = useState<IModal>({ key: null, props: null })

  const showModal = (key: string, props?: unknown) => {
    setModalData({ key, props })
  }

  const hideModal = () => {
    setModalData({ key: null })
  }
  const Modal = modalData.key ? MODALS_CONTENT[modalData.key] : (): null => null

  return (
    <ModalContext.Provider value={{ showModal, hideModal }}>
      {children}
      {modalData.key && (
        <ModalWrapper modal={modalData.key}>
          <Modal {...modalData.props} />
        </ModalWrapper>
      )}
    </ModalContext.Provider>
  )
}

export default ModalProvider
