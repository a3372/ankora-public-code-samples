import classNames from 'classnames'
import React from 'react'
import CloseModalIcon from '../../assets/svg/white-close-circle.svg'
import { useModal } from '../../hooks/modal'
import styles from './Modal.module.scss'

const cn = classNames.bind(styles)

interface IProps {
  children: JSX.Element
  modal?: string
}
const ModalWrapper = ({ children, modal }: IProps): JSX.Element => {
  const { hideModal } = useModal()

  return (
    <>
      {children && (
        <div className={styles.modal_wrapper}>
          <div className={cn(styles.modal_content, { [styles.overflow_modal]: modal === 'SUBSCRIBE_MODAL' })}>
            <div className={styles.modal_close}>
              <img onClick={() => hideModal()} src={CloseModalIcon} alt="Close modal icon" />
            </div>
            {children}
          </div>
          <div className={styles.modal_background_blur} />
        </div>
      )}
    </>
  )
}

export default ModalWrapper
