import CareerModal from './CareerModal'
import NewsletterModal from './NewsletterModal'
import SubmittedModal from './SubmittedModal'

export enum MODALS {
  CAREER_MODAL = 'TEST',
  SUBMITED_MODAL = 'SUBMITED_MODAL',
  SUBSCRIBE_MODAL = 'SUBSCRIBE_MODAL',
}

export const MODALS_CONTENT = {
  [MODALS.CAREER_MODAL]: CareerModal,
  [MODALS.SUBMITED_MODAL]: SubmittedModal,
  [MODALS.SUBSCRIBE_MODAL]: NewsletterModal,
}

export interface MODALS_TYPES {
  [MODALS.CAREER_MODAL]: { message: string }
  [MODALS.SUBMITED_MODAL]: { null: unknown }
  [MODALS.SUBSCRIBE_MODAL]: { image: true }
}
