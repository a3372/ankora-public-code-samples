import classNames from 'classnames'
import React from 'react'
import ChatBox from '../../assets/svg/chat-box-career.svg'
import CircleLeft from '../../assets/svg/circle-left.svg'
import { useModal } from '../../hooks/modal'
import { l } from '../../public/locales/translation'
import CareerForm from '../career/CareerForm'
import styles from './Modal.module.scss'

const cn = classNames.bind(styles)

const CareerModal = ({ message }: { message: string }): JSX.Element => {
  const { hideModal } = useModal()
  return (
    <>
      <div className={cn(styles.modal_container)}>
        <div className={styles.modal_container_back} onClick={() => hideModal()}>
          <img src={CircleLeft} alt="button for go back" />
          <h5>{l.backToCareer} </h5>
        </div>
        <div className={styles.modal_container_content}>
          <h3>{l.pleaseFillRequredFields}</h3>
          <CareerForm job={message} />
        </div>
      </div>
      <div className={styles.modal_image}>
        <img src={ChatBox} alt="chatbox image" />
      </div>
    </>
  )
}

export default CareerModal
