import React, { Children, isValidElement, createElement } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames/bind'

import styles from './Box.module.scss'

const cn = classNames.bind(styles)

const BoxTitle = ({ children, className }) => <h3 className={cn('title', className)}>{children}</h3>

BoxTitle.propTypes = {
  className: PropTypes.string,
}

const BoxTitleComponent = ({ children }) => <div className={cn('titleComponentContainer')}>{children}</div>

const BoxDescription = ({ children }) => <div className={cn('descriptionContainer')}>{children}</div>

const BoxContent = ({ children, className }) => <div className={cn('content', className)}>{children}</div>

BoxContent.propTypes = {
  className: PropTypes.string,
}

const Box = ({ className, children, tight, spacing, titleContainerClassName }) => {
  let Title = ''
  let TitleComponent = ''
  let Description = ''
  let Content = ''
  Children.map(children, child => {
    const { type, props } = child
    if (isValidElement(child) && type === BoxTitle) {
      const { children, ...p } = props
      Title = createElement(BoxTitle, p, children)
    }
    if (isValidElement(child) && type === BoxDescription) {
      const { children, ...p } = props
      Description = createElement(BoxDescription, p, children)
    }
    if (isValidElement(child) && type === BoxContent) {
      const { children, ...p } = props
      Content = createElement(BoxContent, p, children)
    }
    if (isValidElement(child) && type === BoxTitleComponent) {
      const { children, ...p } = props
      TitleComponent = createElement(BoxTitleComponent, p, children)
    }
  })

  return (
    <div className={cn('box', { tight, spaceRight: spacing.right, spaceLeft: spacing.left }, className)}>
      <div className={cn('titleContainer', titleContainerClassName)}>
        {Title}
        {TitleComponent}
      </div>
      {Description}
      {Content}
    </div>
  )
}

Box.propTypes = {
  className: PropTypes.string,
  titleContainerClassName: PropTypes.string,
  tight: PropTypes.boolean,
  spacing: PropTypes.shape({
    right: PropTypes.bool,
    left: PropTypes.bool,
  }),
}

Box.defaultProps = {
  spacing: {},
}

Box.Title = BoxTitle
Box.TitleComponent = BoxTitleComponent
Box.Content = BoxContent
Box.Description = BoxDescription

export default Box
