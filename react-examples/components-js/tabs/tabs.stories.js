import React from 'react'
import { storiesOf } from '@storybook/react'

import Tabs, { Tab } from 'components/tabs'

storiesOf('Tabs', module).add('Default', () => {
  return (
    <Tabs>
      <Tab title="Page one">Some content...</Tab>
      <Tab title="Page two">...and some more</Tab>
    </Tabs>
  )
})
