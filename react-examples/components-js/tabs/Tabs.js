import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames/bind'

import ComingSoonBadge from 'components/coming-soon-badge'

import styles from './Tabs.module.scss'
const s = classNames.bind(styles)

const Tabs = ({ defaultTabId, tabId, children, onChange }) => {
  const currentTab = useRef(null)
  const [currentId, setCurrentId] = useState(defaultTabId || '')
  const [slidingBarOffset, setSlidingBarOffset] = useState('')
  const [slidingBarWidth, setSlidingBarWidth] = useState('')

  useEffect(() => {
    React.Children.map(children, (child, index) => {
      if (React.isValidElement(child)) {
        if (child.type === Tab) {
          // When component is loaded, set the title to the first element
          if (
            (!defaultTabId && index === 0) || // pick fist tab if no default specified
            (defaultTabId && child.props.id === defaultTabId)
          ) {
            setCurrentId(child.props.id || child.props.title)
          }
        }
      }
    })
  }, [defaultTabId])

  // Set the position and width of the sliding bar
  useEffect(() => {
    if (currentTab?.current) {
      setSlidingBarOffset(currentTab.current.offsetLeft)
      setSlidingBarWidth(currentTab.current.offsetWidth)
    }
  }, [currentId, currentTab?.current?.offsetLeft, currentTab?.current?.offsetWidth])

  // Set the position and width of the sliding bar
  useEffect(() => {
    tabId !== undefined && setCurrentId(tabId)
  }, [tabId])

  return (
    <div className={s('tabWrapper')}>
      <div className={s('tabNavigation')}>
        {// Render tab buttons for each Tab child
        React.Children.map(children, (child, index) => {
          if (React.isValidElement(child)) {
            if (child.type === Tab) {
              const isSelected = (child.props.id || child.props.title) === currentId
              const isDisabled = !!child.props.isDisabled
              return (
                <a
                  ref={isSelected ? currentTab : null}
                  className={s('tabButton', { isSelected, isDisabled })}
                  onClick={() => {
                    if (isDisabled) return null
                    if (child.props.id) {
                      // controlled tabs wait for parent to update currentId
                      onChange(child.props.id)
                    } else {
                      // uncontrolled tabs set currentId directly
                      setCurrentId(child.props.title)
                    }
                    // enable callback on selected tab
                    child.props.onSelect && child.props.onSelect()
                  }}
                >
                  {child.props.title}
                  {isDisabled && <ComingSoonBadge />}
                </a>
              )
            } else {
              return ''
            }
          }
        })}
        <div
          className={s('slidingBar')}
          style={{
            left: `${slidingBarOffset}px`,
            width: `${slidingBarWidth}px`,
          }}
        ></div>
      </div>
      <div className={s('activeTab')}>
        {// Render tab if the current tab equals the current title
        React.Children.map(children, child => {
          if (React.isValidElement(child)) {
            if (child.type === Tab && (child.props.id || child.props.title) === currentId) {
              const { isDisabled, title, ...childToRender } = child
              return React.cloneElement(childToRender)
            } else {
              return ''
            }
          }
        })}
      </div>
    </div>
  )
}

Tabs.propTypes = {
  defaultTabId: PropTypes.string,
  tabId: PropTypes.string,
  onChange: PropTypes.func,
}

Tabs.defaultProps = {
  onChange: () => {},
}

const Tab = ({ children, className }) => <div className={className}>{children}</div>

Tab.propTypes = {
  title: PropTypes.string,
  id: PropTypes.string,
  className: PropTypes.string,
}

Tab.defaultProps = {
  title: 'Empty title',
}

export { Tabs as default, Tab }
