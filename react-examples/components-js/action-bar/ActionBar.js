import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames/bind'
import { motion, AnimatePresence } from 'framer-motion'
import Portal from 'components/portal'

import styles from './ActionBar.module.scss'

const s = classNames.bind(styles)

const variants = {
  visible: { opacity: 1, y: 0 },
  hidden: { opacity: 0, y: 100 },
}

const ActionBar = ({ children, isOpen, className, ...props }) => {
  return (
    <Portal>
      <AnimatePresence>
        {isOpen && (
          <motion.div
            className={s('actionBar', { [className]: className })}
            initial="hidden"
            animate="visible"
            exit="hidden"
            variants={variants}
            transition={{ duration: 0.3, type: 'tween' }}
            {...props}
          >
            {children}
          </motion.div>
        )}
      </AnimatePresence>
    </Portal>
  )
}

ActionBar.propTypes = {
  isOpen: PropTypes.bool,
  className: PropTypes.string,
}

ActionBar.defaultProps = {
  isOpen: false,
}

export default ActionBar
