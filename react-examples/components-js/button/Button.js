import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames/bind'

import Clickable from 'components/clickable'
import Icon from 'components/icon'

import s from './Button.module.scss'
const cn = classNames.bind(s)

const Button = ({ to, children, className, disabled, color, arrow, icon, size, side, circle, ...rest }) => {
  const content = arrow ? <Icon id="arrow-right" /> : children

  const wrapper = cn('button', className, size, { disabled, arrow, circle, [color]: !disabled && !arrow })

  return (
    <Clickable to={to} disabled={disabled} {...rest} className={wrapper}>
      {icon && side === 'left' && (
        <span className={cn('iconWrap', { circle })}>
          <Icon id={icon} className={cn('icon', size, { [side]: !!content })} />
        </span>
      )}
      {content}
      {icon && side === 'right' && (
        <span className={cn('iconWrap', { circle })}>
          <Icon id={icon} className={cn('icon', size, { [side]: !!content })} />
        </span>
      )}
    </Clickable>
  )
}

Button.propTypes = {
  to: PropTypes.string,
  disabled: PropTypes.bool,
  children: PropTypes.node,
  className: PropTypes.string,
  color: PropTypes.oneOf(['red', 'white', 'transparent', 'borderRed']),
  arrow: PropTypes.bool,
  circle: PropTypes.bool,
  icon: PropTypes.string,
  size: PropTypes.oneOf(['large', 'default', 'small']),
  type: PropTypes.oneOf(['button', 'submit']),
  side: PropTypes.oneOf(['left', 'right']),
}

Button.defaultProps = {
  color: 'red',
  arrow: false,
  circle: false,
  disabled: false,
  size: 'default',
  type: 'button',
  side: 'left',
}

export default Button
