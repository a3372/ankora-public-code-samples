import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { select } from '@storybook/addon-knobs'

import Button from 'components/button' // add dynamic UI variables

storiesOf('Button', module)
  .add('Default', () => {
    const size = select('Size', ['small', 'default', 'large'], 'default')
    const type = select('Type', ['button', 'submit'])
    const colour = select('Colour', ['red', 'white', 'transparent', 'borderRed'], 'red')

    return (
      <Button disabled={false} size={size} type={type} color={colour} onClick={action('clicked')}>
        Simple button
      </Button>
    )
  })
  .add('Disabled', () => {
    const size = select('Size', ['small', 'default', 'large'], 'default')
    const type = select('Type', ['button', 'submit'])
    const colour = select('Colour', ['red', 'white', 'transparent', 'borderRed'], 'red')

    return (
      <Button disabled={true} size={size} type={type} color={colour} onClick={action('clicked')}>
        Simple button
      </Button>
    )
  })
