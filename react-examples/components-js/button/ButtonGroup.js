import React from 'react'
import PropTypes from 'prop-types'
import className from 'classnames/bind'

import s from './Button.module.scss'
const cn = className.bind(s)

const ButtonGroup = ({ children, isFlex, className, style }) => (
  <div className={cn('buttonGroup', className, { isFlex })} style={style}>
    {children}
  </div>
)

ButtonGroup.propTypes = {
  className: PropTypes.string,
  isFlex: PropTypes.bool,
  style: PropTypes.object,
}

ButtonGroup.defaultProps = {
  isFlex: false,
}

export default ButtonGroup
